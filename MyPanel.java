import java.awt.Graphics;
import javax.swing.JPanel;
import java.awt.image.*;
import java.awt.event.KeyListener;
import java.awt.Color;
import java.awt.event.KeyEvent;

// LED whiteboard - NJM

// just your basic drawing panel...
public class MyPanel extends JPanel implements KeyListener{
  BufferedImage theImg; // other things draw into this

  private int lastX,lastY; // coords of (center of) last detected dot
  private boolean doDraw=true; // default

  int X0=100,Y0=100,X1=500,Y1=400; // screen coords
  int CX0=100,CY0=100,CX1=500,CY1=400; // camera coords (will be set)

  boolean imageEcho=false;
  boolean showButtons=false;

  public MyPanel(BufferedImage theImg)
  {
    this.theImg=theImg;
    this.setFocusable(true);
    addKeyListener(this);
  }

  public void saveLastXY(int lastX,int lastY)
  {
    this.lastX=lastX;
    this.lastY=lastY;
  }

  public void paint(Graphics g)
  {
// draw theImg
    if (doDraw){ // normal mode: show detected dot locations
      g.drawImage(theImg,0,0,this);
    }
  }

  public void keyPressed(KeyEvent event){}
  public void keyReleased(KeyEvent event){}
/*
 * Q - quit
 * c - clear screen
 * 1 - light UL dot
 * 2 - register UL
 * 3 - light LR dot
 * 4 - register LR
 * n - normal mode
 * i - toggle image echo
 * ? - toggle button display
 */

  public void keyTyped(KeyEvent event)
  {
    char c=event.getKeyChar();
    switch(c){
      case 'Q': System.exit(0);
      case 'c': // clear
        clear();
        return;
      case 'n': doDraw=true;return; // not really used lol
      case '1': clear();bullseye(X0,Y0,theImg);return; // display registration mark
      case '2': CX0=lastX;CY0=lastY;clear();return;
      case '3': clear();bullseye(X1,Y1,theImg);return;
      case '4': CX1=lastX;CY1=lastY;clear();return;
      case 'i': imageEcho=!imageEcho;return;
      case '?': showButtons=!showButtons;clear();repaint();return;
      default: System.out.println(c + "?");
    }
  }

// draw a target at given centerpoint
  void bullseye(int x, int y, BufferedImage img)
  {
    Graphics g=img.getGraphics();
    g.setColor(Color.GREEN);
    g.fillOval(x-1,y-1,3,3);
    g.drawOval(x-5,y-5,11,11);
    g.drawOval(x-10,y-10,21,21);
    g.drawLine(x-13,y,x+13,y);
    g.drawLine(x,y-13,x,y+13);
  }

// show buttons
  void showButtons()
  {
    Graphics g=theImg.getGraphics();
// show CLEAR button
    g.setColor(new Color(0,255,255));
    g.fillOval(0,0,30,30);
    g.drawString("C",15,45);

// show color swatches
    g.setColor(new Color(0,255,0));
    g.fillOval(610,0,30,30);
    g.setColor(new Color(0,255,255));
    g.fillOval(610,450,30,30);
    repaint();
  }

  void clear() // clear panel
  {
    for (int x=0;x<theImg.getWidth();x++){
      for (int y=0;y<theImg.getHeight();y++){
        theImg.setRGB(x,y,0x000000);
      }
    }
    if (showButtons) showButtons();
    repaint();
  }
}
