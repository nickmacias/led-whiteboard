import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.image.*;
import java.awt.GraphicsEnvironment;
import java.awt.GraphicsDevice;
import java.awt.GraphicsConfiguration;
import java.awt.Transparency;

// LED whiteboard - NJM
// Started around 2012, now it's Dec 2018 lol
//
// Connect a projector and put a cam facing the image
// Code will detect bright red (i.e. a laser pointer)
// and draw in turquoise (or green) wherever it finds a dot
// Webcam capture uses 640x480 (can extend to 1280x720 but
// speed becomes an issue).
//
// System needs to be calibrated. Once running (wait a few sec
// for system to initialize), hit a 1, which should display
// a bullseye near tghe upper left. Point the laser at that location
// and hit a 2. Repeat with a 3 (lower right) and 4 (register).
// c clears the canvas, Q quits immediately.
// i toggles a live image display (persistant after turning off: clear with c)
// ? toggles display of the hidden buttons
//

public class Main extends JFrame {

  //int WIDTH=1280;
  //int HEIGHT=720;
  int WIDTH=640;
  int HEIGHT=480;

  private JPanel contentPane;
  public BufferedImage theImg;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          Main frame = new Main();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public Main() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setExtendedState(JFrame.MAXIMIZED_BOTH);
    setUndecorated(true); // no border etc.
    setBackground(Color.DARK_GRAY);

    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    contentPane.setBackground(Color.GRAY);
    
    theImg=new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB); // draw here

    MyPanel panel = new MyPanel(theImg);
    panel.setBounds(0,0,WIDTH,HEIGHT);
    contentPane.add(panel);
    panel.setDoubleBuffered(true);
    panel.clear();

// start the capture thread
    Capture c=new Capture(theImg,panel);
    c.start();
  }
}
