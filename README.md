Pet project from several years ago. A whiteboard-like system using a
laser pointer for drawing and control.

A camera monitors the drawing surface, notes when the laser is pointing at it,
and records the information in a Java BufferedImage.

The image is continually displayed by a projector onto the drawing surface.

Calibration is done with a set of keystroke commands.

Keyboard commands can also be used to clear, quit, display the
background image, and display hidden buttons.

Hidden buttons allow clearing and changing the ink color by pointing.

.jar files are not mine!
