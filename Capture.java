import java.awt.*;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import com.github.sarxos.webcam.Webcam;
import java.awt.image.BufferedImage;
import javax.swing.border.EmptyBorder;
import com.github.sarxos.webcam.WebcamResolution;

// LED whiteboard - NJM

public class Capture extends Thread {

  BufferedImage theImg;
  MyPanel panel;
  int lastsx=-1,lastsy=-1; // for drawing lines into theiImg
  boolean imageEcho=false;

  int buttonRadius=15; // what it is

  int thisColor=0; // rotate through colors
  int WIDTH,HEIGHT; // resolution and panel size
  int DOTWIDTH=1; // really 2*(DOTWIDTH)-1 :-P
  //int CWIDTH=1280,CHEIGHT=720; // Camera dims
  int CWIDTH=640,CHEIGHT=480; // Camera dims

  int lastX,lastY; // coords of (center of?) last captured dot

// Grab image from camera, draw on BufferedImage and repaint panel
// Also handle special commands (virtual buttons)

  public Capture(BufferedImage theImg,MyPanel panel)
  {
    WIDTH=panel.getWidth();HEIGHT=panel.getHeight();
    this.theImg=theImg;
    this.panel=panel;
  }

// run as a separate thread
  public void run(){
    int x,y,c,r,g,b,a;
    BufferedImage image;

    Webcam wc = Webcam.getWebcams().get(1); // USB camera
    //Webcam wc = Webcam.getWebcams().get(0); // built-in cam
    //wc.setCustomViewSizes(new Dimension[]{WebcamResolution.HD720.getSize()}); // register custom size
    //wc.setViewSize(new Dimension(1280,720));
    wc.setViewSize(new Dimension(WIDTH,HEIGHT)); // works reasonably well in a 1980s kind of way

    wc.open(true); // non-buffering! (didn't help speed)
    System.out.println("Webcam: " + wc.getName());

    while (true){
      image = wc.getImage(); // get image  
    
// sweep the image and find the red dot. Average the x and y coords
// to reduce the dot to a single pixel (or not lol)

      int xSum=0, ySum=0, numPixels=0;
      int lX=-1,lY=-1,maxR=0;

      for (y=0;y<CHEIGHT;y++){  
        for (x=0;x<CWIDTH;x++){  
          c=image.getRGB(x,y);  
          b=c&0xff;  
          g=(c>>8)&0xff;  
          r=(c>>16)&0xff;  
          //a=(c>>24)&0xff; // don't need alpha
    
          if (panel.imageEcho){ // hahaha - image echo
            theImg.setRGB(x,y,c);
            //continue;
          }

          //System.out.println(x + "," + y+" "+r+" "+g+" "+b);
          if ((r-g > 64) && (r-b > 64) && (r > 128)){ // should be bright and mainly red
            doScaledDraw(x,y,color()); // remove for single-point drawing at
                                      // place of maximum intensity
            if (r > maxR){ // record brightest spot
              lX=x;lY=y;maxR=r; // location and intensity
            }
          }
        }  
      }  
      if (panel.imageEcho) panel.repaint();

// now find points close to the maximum brightness
/*** doesn't really help much, so skip this
      for (y=0;y<CHEIGHT;y++){  
        for (x=0;x<CWIDTH;x++){  
          c=image.getRGB(x,y);  
          b=c&0xff;  
          g=(c>>8)&0xff;  
          r=(c>>16)&0xff;  
          if ((r-g > 64) && (r-b > 64) && (r > 128) && (r > maxR-16)){
            doScaledDraw(x,y,color());
          }
        }
      }
***/

// record last point (mainly for calibration)
      if (lX >= 0){
        lastX=lX;lastY=lY;
        //doScaledDraw(lastX,lastY,color());
        panel.saveLastXY(lastX,lastY);
        panel.repaint();
      }

      yield(); // let Swing have a shot...
    }  
  }

// return current color
  int color()
  {
    switch(thisColor){
      case 0: return(0x00ffff);
      case 1: return(0x00ff00);
      case 2: return(0x0000ff);
      default: return(0);
    }
  }

// rotate colors (not used)
  void nextColor()
  {
    thisColor=(++thisColor)%2;
  }

// scale dot image based on calibration and draw corresponding point
  private void doScaledDraw(int x, int y, int color)
  {
    if (panel.CX1==panel.CX0) return; // bogus...
    int sx=panel.X0+((x-panel.CX0)*(panel.X1-panel.X0))/(panel.CX1-panel.CX0);
    int sy=panel.Y0+((y-panel.CY0)*(panel.Y1-panel.Y0))/(panel.CY1-panel.CY0);
    if ((sx < WIDTH) && (sx >= 0) &&  // if we light a region around centerpoint, these
        (sy < HEIGHT) && (sy >= 0)){ // could go negative...
      theImg.setRGB(sx,sy,color); // first paint the point
      if (proximity(sx,sy,buttonRadius,buttonRadius,buttonRadius))
         panel.clear(); // clear - upper left
      if (proximity(sx,sy,WIDTH-buttonRadius,buttonRadius,buttonRadius))
         thisColor=1;  // color 1, upper-right
      if (proximity(sx,sy,WIDTH-buttonRadius,HEIGHT-buttonRadius,buttonRadius))
         thisColor=0; // color 0, lower-right

/*** // continuous lines...
      theImg.getGraphics().setColor(new Color(color));
      if (lastsx >= 0){
        theImg.getGraphics().drawLine(lastsx,lastsy,sx,sy);
      }
***/
      lastsx=sx;lastsy=sy;
    }
  }

  boolean proximity(int x, int y, int x0, int y0, int r)
  {
    return(Math.sqrt((double)((x-x0)*(x-x0)+(y-y0)*(y-y0))) <= r);
  }
}
